package com.sirarat.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    var btn1: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val name : TextView = findViewById(R.id.textViewName)
        val nameString : String = name.text.toString()
        val code : TextView = findViewById(R.id.ShowCode)
        val codeString : String = code.text.toString()
        btn1  = findViewById<Button>(R.id.btn1)

       btn1!!.setOnClickListener {
           val intent = Intent(this,HelloActivity::class.java)
           intent.putExtra("Name" , nameString)
           intent.putExtra("Code" , codeString)
           startActivity(intent)
       }
    }


}